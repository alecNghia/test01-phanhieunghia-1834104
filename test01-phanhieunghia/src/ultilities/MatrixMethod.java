package ultilities;

import java.util.Arrays;

public class MatrixMethod {
	public static int[][] duplicate(int[][] oldArr) {
		int[][]newArr = new int[oldArr.length][oldArr[1].length*2];
		int count = 0; //count for the array index to be repeated
		for (int i = 0; i < newArr.length; i++) {
			for (int j = 0; j < newArr[i].length; j++, count++) {
				newArr[i][j] = oldArr[i][count]; 
				if(count == 2) { //if count hit the third index
					count = -1;  //it will be put back to index = 0 (since count++)
				}
			}
		}
		return newArr;
	}
}
