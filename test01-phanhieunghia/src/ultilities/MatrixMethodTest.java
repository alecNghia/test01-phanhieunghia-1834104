package ultilities;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MatrixMethodTest {

	@Test
	//test for the matrix
	void duplicateTest() {
		int[][] rec = new int[][] {{1,2,3},{4,5,6}};
		int[][] newRec = new int[][] {{1,2,3,1,2,3},{4,5,6,4,5,6}};
		int[][] updatedRec = MatrixMethod.duplicate(rec);
		assertArrayEquals(newRec, updatedRec);
	}
}
