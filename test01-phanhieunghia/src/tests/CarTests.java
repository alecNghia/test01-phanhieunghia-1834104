package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import vehicles.Car;

class CarTests {

	@Test
	//test if the constructor works
	void carConstructorTest() {
		try {
			Car honda = new Car(-10);
		}
		catch (Exception IllegalArgumentException) {
			System.err.println("Invalid input");
		}
	}
	
	@Test
	//test if the speed of the car
	void getSpeedTest() {
		Car madza = new Car(50);
		assertEquals(50, madza.getSpeed());
	}
	
	@Test
	// test if the car move to the right
	void moveRightTest() {
		Car huyndai = new Car(70);
		huyndai.moveRight();
		assertEquals(70, huyndai.getLocation());
	}
	
	@Test
	// test if the car move to the left
	void moveLeftTest() {
		Car audi = new Car(45);
		audi.moveLeft();
		assertEquals(-45, audi.getLocation());
	}
	
	@Test
	// test to see the location of the car moving right
	void getRightLocation() {
		moveRightTest();
	}
	
	@Test
	// test to see the location of the car moving left
	void getLeftLocation() {
		moveLeftTest();
	}
	
	@Test
	// test for the accelerating speed
	void accelerateTest() {
		Car lamborghini = new Car(100);
		lamborghini.accelerate();
		assertEquals(101, lamborghini.getSpeed());
	}
	
	@Test
	// test for decelerate speed
	void decelerateTest() {
		Car bugatti = new Car(125);
		bugatti.decelerate();
		assertEquals(124, bugatti.getSpeed());
	}
}
	
